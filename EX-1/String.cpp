#include "String.h"


/* -- value -- */
String::String(const std::string& val) :
	Sequence(), _val(val)
{
}


//? Methods ?//


/* -- is printable -- */
bool String::isPrintable() const
{
	return true;
}

/* -- return string value -- */
std::string String::toString() const
{
	if (_val.find("'") != std::string::npos)
	{
		return R"(")" + _val + R"(")";
	}
	else
	{
		return "'" + _val + "'";
	}
}

std::string String::getTypeName() const
{
	return "str";
}

int String::getLen() const
{
	return _val.size();
}
