#ifndef INTEGER_H
#define INTEGER_H

#include "Type.h"


class Integer : public Type
{
public:
	// constructors
	Integer(const int& val);
	virtual ~Integer() = default;

	// methods
	virtual bool isPrintable() const override;
	virtual std::string toString() const override;
	virtual std::string getTypeName() const override;

private:
	int _val;
};

#endif // INTEGER_H;