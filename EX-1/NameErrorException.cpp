#include "NameErrorException.h"


/* -- c'tor -- */
NameErrorException::NameErrorException(const std::string& name) :
    _msg("NameError: name '" + name + "' is not defined")
{
}



const char* NameErrorException::what() const throw()
{
    return _msg.c_str();
}
