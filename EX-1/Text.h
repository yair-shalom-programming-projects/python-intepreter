#pragma once
#include "String.h"

class Text : public String
{
public:
	// constructors
	Text(const std::string& txt);
	virtual ~Text() = default;

	// methods
	virtual std::string toString() const override;
	virtual int getLen() const override;


};

