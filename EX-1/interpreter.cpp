#include "type.h"
#include "InterperterException.h"
#include "parser.h"
#include <iostream>

constexpr const char* WELCOME = "Welcome to Magshimim Python Interperter version 1.0 by ";
constexpr const char* YOUR_NAME = "Yair Shalom";

using std::cout;
using std::cin;
using std::endl;
using std::string;
using std::shared_ptr;


int main(int argc, char **argv)
{

	cout << WELCOME << YOUR_NAME << endl;

	string input_string;
	shared_ptr<Type> var;

	do
	{
		cout << ">>> ";
		std::getline(cin, input_string);

		try
		{
			// prasing command
			if ((var = Parser::parseString(input_string)) && var->isPrintable())
			{
				cout << var->toString() << endl;
			}
		}
		catch (const std::exception& e)
		{
			cout << "error: " << e.what() << endl;
		}


	} while (input_string != "quit()");



}


