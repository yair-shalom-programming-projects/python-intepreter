#include "Void.h"



//? Methods ?//


/* -- return false - void isn't printable -- */
bool Void::isPrintable() const
{
    return false;
}

std::string Void::toString() const
{
    return std::string();
}

std::string Void::getTypeName() const
{
    return std::string();
}
