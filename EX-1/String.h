#pragma once

#include "Sequence.h"
#include "String.h"

class String : public Sequence
{
public:
	// constructors
	String(const std::string& val);
	virtual ~String() = default;

	// methods
	virtual bool isPrintable() const override;
	virtual std::string toString() const override;
	virtual std::string getTypeName() const override;
	virtual int getLen() const override;

protected:
	std::string _val;
};