#pragma once
#include "type.h"
#include "Sequence.h"
#include "List.h"
#include <string>
#include <memory>
#include <list>


class List : public Sequence
{
public:
	// constructors
	List(std::string list);
	virtual ~List() = default;

	// methods
	virtual bool isPrintable() const override;
	virtual std::string toString() const override;
	virtual std::string getTypeName() const override;
	virtual int getLen() const override;

private:
	std::list<std::shared_ptr<Type>> _var;
};