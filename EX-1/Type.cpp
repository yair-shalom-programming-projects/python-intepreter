#include "type.h"


//? Constructors ?//

/* -- C'tor -- */
Type::Type() :
    _isTemp(false)
{
}




//? Setters ?//

/* -- set _isTemp -- */
void Type::setTemp(const bool isTemp)
{
    _isTemp = isTemp;
}


//? Getters ?//

/* -- get _isTemp -- */
bool Type::isTemp() const
{
    return _isTemp;
}
