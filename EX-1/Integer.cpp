#include "Integer.h"

/* -- c'tor -- */
Integer::Integer(const int& val) :
	Type(), _val(val)
{
}


//? Methods ?//

/* -- return if value is printable -- */
bool Integer::isPrintable() const
{
	return true;
}


/* -- change value to string type -- */
std::string Integer::toString() const
{
	return std::to_string(_val);
}

std::string Integer::getTypeName() const
{
	return "int";
}
