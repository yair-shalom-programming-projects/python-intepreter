#include "Boolean.h"


/* -- c'tor -- */
Boolean::Boolean(const bool val) :
	Type(), _val(val)
{
}

//? Methods ?//

/* -- is value printable -- */
bool Boolean::isPrintable() const
{
	return true;
}

/* -- change value to string -- */
std::string Boolean::toString() const
{
	return _val ? "True" : "False";
}

std::string Boolean::getTypeName() const
{
	return "bool";
}

