#pragma once

#include "Type.h"


class Sequence : public Type
{
public:
	
	Sequence() = default;
	virtual ~Sequence() = default;
	// methods
	virtual bool isPrintable() const = 0;
	virtual std::string toString() const = 0;
	virtual std::string getTypeName() const = 0;
	virtual int getLen() const = 0;

};

