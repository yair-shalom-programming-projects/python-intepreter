#ifndef VOID_H
#define VOID_H
#include "type.h"

class Void : public Type
{
public:
	// methods
	virtual bool isPrintable() const override;
	virtual std::string toString() const override;
	virtual std::string getTypeName() const override;

};
#endif // VOID_H;