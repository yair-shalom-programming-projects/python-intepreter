#include "parser.h"
#include "Helper.h"

#include "IndentationException.h"
#include "SyntaxException.h"
#include "NameErrorException.h"

#include "Boolean.h"
#include "String.h"
#include "Integer.h"
#include "List.h"
#include "Text.h"
#include "Void.h"

#include <iostream>
#include <map>


using std::map;
using std::string;
using std::shared_ptr;
using std::make_shared;

std::unordered_map<string, shared_ptr<Type>> Parser::_variables = {};


/*
	-- parse string into python code --
	* input: line of code
	* output: type of variable created (shared_ptr to Type)
*/
shared_ptr<Type> Parser::parseString(string str)
{

	if (str[0] == ' ' || str[0] == '\t')
		throw IndentationException();
	
	// remove spaces from the end
	Helper::rtrim(str);

	shared_ptr<Type> variable;


	if ( (variable = getType(str)) )
	{
	}
	else if ( !str.find("type(") && str[str.size()-1] == ')' )
	{
		variable = make_shared<Text>(typeFunc(str));
	}
	else if ( !str.find("del") )
	{
		variable = make_shared<Void>();
		delFunc(str);
	}
	else if ( !str.find("len(") && str[str.size()-1] == ')' )
	{
		variable = make_shared<Integer>(lenFunc(str));
	}
	else if ( makeAssignment(str) )
	{
		variable = make_shared<Void>();
	}
	else if ( (variable = getVariableValue(str)) )
	{
		string helper = variable->toString();
		variable = getType(helper);
	}
	else if ( isLegalVarName(str) )
	{
		throw NameErrorException(str);
	}
	else
	{
		throw SyntaxException();
	}

	variable->setTemp(true);
	return variable;

}


/* -- get Type of variable -- */
shared_ptr<Type> Parser::getType(string& str) 
{

	Helper::trim(str);
	Helper::removeLeadingZeros(str);
	shared_ptr<Type> type;


	if (str.empty()) {

		return nullptr;
	}
	else if (Helper::isBoolean(str))
	{
		type = make_shared<Boolean>(str == "True");
	}
	else if (Helper::isInteger(str))
	{
		type = make_shared<Integer>(stoi(str));
	}
	else if (Helper::isString(str))
	{
		type = make_shared<String>(str.substr(1, str.size() - 2));
	}
	else if (Helper::isList(str))
	{
		type = make_shared<List>(str);
	}
	else
	{
		return nullptr;
	}

	type->setTemp(true);

	return type;
}



/*
	-- find if variable name is legal --
	* input: string (line of code)
	* output: bool - is legal
*/
bool Parser::isLegalVarName(const string& str)
{
	bool firstLetter = true;
	
	for (auto& let : str)
	{
		if ((!Helper::isUnderscore(let) && !Helper::isLetter(let) && !Helper::isDigit(let)))
			return false;

		if (firstLetter && Helper::isDigit(let))
			return false;

		firstLetter = false;
	}
	return true;
}


/*
	-- get variable value --
	* input: str - line of code
	* output: assignment was made or not 
	* exception: if name/type is ilegal
*/
bool Parser::makeAssignment(const string& str)
{
	size_t equal = str.find('=');

	if (equal == string::npos || equal == str[0] || equal == str[str.size() - 1])
		return false;

	string varName, value;

	varName = str.substr(0, equal);
	value = str.substr(equal + 1, str.size() - equal + 1);

	Helper::rtrim(varName);
	Helper::trim(value);


	if (!isLegalVarName(varName))
		throw SyntaxException();


	shared_ptr<Type> var;

	// find if type valid
	if ((var = getType(value)))
	{
		// add variable to _variables
		_variables[varName] = var;
	}
	else if (isLegalVarName(value))
	{
		copy(varName, value);
	}
	else
	{
		throw SyntaxException();
	}
	return true;

}


/* -- get variable value (if is not exist return nullptr) -- */
shared_ptr<Type> Parser::getVariableValue(const string& str)
{
	if (_variables.count(str))
		return _variables[str];
	return nullptr;
	
}

/*
	-- preform deep copy --
	* input: dest str (new var), src str (var to copy his value )
*/
void Parser::copy(string& dest, string& src)
{
	auto srcType = getVariableValue(src);
	shared_ptr<Type> destType = nullptr;

	if (srcType)
	{
		if (Helper::isList(src))
		{
			// shallow copy
			destType = srcType;
		}
		else
		{
			// deep copy
			string helper = srcType->toString();
			destType = getType(helper);
		}

		_variables[dest] = destType;
	}
	else
	{
		throw NameErrorException(src);
	}
}

void Parser::delFunc(string str)
{
	// remove 'del'
	Helper::trim(str = (str.substr(3)));

	if (!_variables[str])
		throw NameErrorException(str);

	_variables.erase(str);

}

int Parser::lenFunc(string str)
{

	// remove 'len(' and ')'
	str = str.substr(4, str.size() - 1 - 4);


	// find if the type is sequence
	if (auto seq = std::dynamic_pointer_cast<Sequence>(getVariableValue(str)))
		return seq->getLen();
	
	// if the Type is not sequence - throw syntax error
	throw SyntaxException();
	

}



/*
	-- implementation of type function --
	* input: string in current formant:
*/
string Parser::typeFunc(string str)
{

	const int TYPE_FUNC_LEN = 5;

	// remove "type(" and ")"
	str = str.substr(TYPE_FUNC_LEN, str.size() - TYPE_FUNC_LEN - 1);

	auto type = Parser::getVariableValue(str);

	if (!type)
		throw NameErrorException(str);
	
	return string("<type '" + type->getTypeName() + "'>");
}