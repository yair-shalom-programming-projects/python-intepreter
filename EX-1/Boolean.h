#pragma once

#include "type.h"

class Boolean : public Type
{
public:
	// constructors
	Boolean(const bool val);
	virtual ~Boolean() = default;

	// methods
	virtual bool isPrintable() const override;
	virtual std::string toString() const override;
	virtual std::string getTypeName() const override;


private:
	bool _val;
};

