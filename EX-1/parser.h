#pragma once

#include "InterperterException.h"
#include "type.h"
#include "Helper.h"
#include <string>
#include <unordered_map>
#include <iostream>
#include <sstream>
#include <memory>
#include <unordered_map>


class Parser
{

public:
	static std::shared_ptr<Type> parseString(std::string str);
	static std::shared_ptr<Type> getType(std::string &str);

private:

	// private methods
	static bool isLegalVarName(const std::string& str);
	static bool makeAssignment(const std::string& str);
	static std::shared_ptr<Type> getVariableValue(const std::string &str);
	static std::string typeFunc(std::string str);
	static void copy(std::string& str, std::string& var);
	static void delFunc(std::string str); 
	static int lenFunc(std::string str);
	
	// fields
	static std::unordered_map<std::string, std::shared_ptr<Type>> _variables;

};

