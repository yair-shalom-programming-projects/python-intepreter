#include "List.h"
#include "String.h"
#include "parser.h"
#include "SyntaxException.h"

using std::string;
using std::shared_ptr;


List::List(string list) :
	Sequence()
{
	// remove '[' and ']'
	list = list.substr(1, list.size()-2);
	Helper::trim(list);

	if (list[0] == ',' || list[list.size() - 1] == ',')
		throw SyntaxException();


	// convert list string to list 
	std::istringstream iss(list);
	std::string currentVar;
	shared_ptr<Type> type ;


	while ( std::getline(iss, currentVar, ',') ) 
	{
		// remove spaces
		Helper::trim(currentVar);

		// if current variable is not valid - throw exception
		if ( !(type = Parser::getType(currentVar)) )
			throw SyntaxException();

		_var.push_back(type);
	}
	
}

bool List::isPrintable() const
{
	return true;
}

string List::toString() const
{
	string txt = "[";

	for (auto& currentVar : _var)
		txt += currentVar->toString() + ", ";
	
	// remove ", " added to the end of txt string
	return txt.substr(0,txt.size()-2) + "]";
	
}

std::string List::getTypeName() const
{
	return "list";
}

int List::getLen() const
{
	return _var.size();
}
