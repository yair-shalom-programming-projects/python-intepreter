#ifndef TYPE_H
#define TYPE_H

#include <string>


class Type
{

public:
	// constructors
	Type();
	virtual ~Type() = default;
	
	//setters
	virtual void setTemp(const bool isTemp);

	//getters
	virtual bool isTemp() const;

	// methods
	virtual bool isPrintable() const = 0;
	virtual std::string toString() const = 0;
	virtual std::string getTypeName() const = 0;

private:
	//fields
	bool _isTemp;

};





#endif //TYPE_H
